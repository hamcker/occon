import { Component } from '@angular/core';
import { UserBid } from './models/userBid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  bidders: String[] = ["Samaneh", "Hamed", "Ali"]
}
