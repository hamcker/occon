export class BalanceTime {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;

  static fromMilliseconds(inp : number) : BalanceTime {
    var outlet = new BalanceTime();
    var days = inp / (1000 * 3600 * 24);
    outlet.days = Math.floor(days);
    var remain = inp - (outlet.days * 24 * 3600 * 1000)
    var hours = remain / (1000 * 3600);
    outlet.hours = Math.floor(hours);
    remain = remain - (outlet.hours * 1000 * 3600);
    var minutes = remain / (1000 * 60);
    outlet.minutes = Math.floor(minutes);
    remain = remain - (outlet.minutes * 60000);
    outlet.seconds = Math.floor(remain / 1000);

    return outlet;
  }
}