import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { UserBid } from './models/userBid';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class BidService {

  private highestBidSource = new Subject<UserBid>();
  public highestBid = this.highestBidSource.asObservable();

  constructor(
    private httpClient: HttpClient,
    private socketService: SocketService) {
    this.getBids().subscribe(res => {
      this.setHighest(res.sort(this.userBidComparer));
    });

    socketService.socket.on('newBids', data => {
      this.setHighest(data);
    })
  }

  public placeBid(name: String, mount: Number): void {
    this.httpClient
      .post<UserBid[]>('api', { name, mount })
      .subscribe(res => {
        this.setHighest(res)
        this.socketService.socket.emit('newBids', res);
      });

    var userBidInfo : UserBid = new UserBid();
    userBidInfo.name = name;
    userBidInfo.mount = mount;
    console.log('sending userBidInfo...')
    this.socketService.socket.emit('userBidInfo', userBidInfo);
  }

  public getBids(): Observable<UserBid[]> {
    return this.httpClient
      .get<UserBid[]>('api');
  }

  private setHighest(topBidders: UserBid[]): void {
    topBidders[0].payMount = topBidders[1].mount.valueOf() + 0.01;
    this.highestBidSource.next(topBidders[0]);
  }

  private userBidComparer(a: UserBid, b: UserBid): number {
    if (a.mount < b.mount) return 1;
    if (b.mount < a.mount) return -1;
    return 0;
  }
}
