# Occon

Done by Hamed Zakeri Miyab (hamcker@gmail.com)

To run the project please do the followings:

1. Install npm modules:
```shell
> npm i
```

2. Run the server
```shell
> npm run run
```

3. Browse to [localhost:4200](http://localhost:4200) in your browser (chrome and firefox are preferred).