import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceDisplayerComponent } from './balance-displayer.component';

describe('BalanceDisplayerComponent', () => {
  let component: BalanceDisplayerComponent;
  let fixture: ComponentFixture<BalanceDisplayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceDisplayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
