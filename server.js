// blocks
const express = require('express');
const mongo = require('mongoose');
const path = require('path');
const cors = require('cors');


// Initialization
mongo.connect('mongodb://localhost:27017/occon', { useNewUrlParser: true });
const userBidSchema = new mongo.Schema({
    name: String,
    mount: { type: Number, default: 0 },
    offered: Date,
    isHighest: Boolean,
    payMount: Number
});
var userBid = mongo.model('userBid', userBidSchema);

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'dist/occon')));
app.use(cors());

var http = require('http').Server(app);
var io = require('socket.io')(http);

app.route('/api')
    .get((req, res) => {
        userBid.find({}, (err, docs) => {
            if (err)
                res.status(400).json(err);
            else
                res.status(200).json(docs);
        });
    })
    .post((req, res) => {
        userBid.find({ name: req.body.name }, (err, docs) => {
            var userbid;
            if (err)
                res.status(500).json(err);
            else if (docs.length) {
                userbid = docs[0];
                userbid.mount = req.body.mount;
                userbid.offered = new Date();
            }
            else {
                var userbid = new userBid();
                userbid.name = req.body.name;
                userbid.mount = req.body.mount;
                userbid.offered = new Date();
            }

            userbid.save((err, obj) => {
                if (err) { res.status(400).json(err); return; }

                userBid.find().sort({ mount: -1 }).limit(2).exec((err, topBidders) => {
                    if (err) { res.status(400).json(err); return; }
                    res.status(200).json(topBidders);
                });
            })


        })
    })

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/occon/index.html'));
});

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
    socket.on('newBids', function(data){
        io.emit('newBids', data);
      });
    socket.on('userBidInfo', function(data){
        io.emit('userBidInfo', data);
      });
});

http.listen(4200, () => { console.info('express started on port 4200') });

