import { UserBid } from "./userBid";
import { BalanceTime } from "./balanceTime";

export class Auction {
  highestUserBid: UserBid;
  deadline : Date;
  balanceTime : BalanceTime;
}