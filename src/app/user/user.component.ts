import { Component, OnInit, Input } from '@angular/core';
import { BidService } from '../bid.service';
import { UserBid } from '../models/userBid';
import { SocketService } from '../socket.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(
    private bidService: BidService,
    private socketService: SocketService) {
    this.user = new UserBid();
  }

  ngOnInit() {
    this.user.name = this.name;
    this.bidService.highestBid.subscribe((res: UserBid) => {
      this.user.isHighest = this.user.name === res.name
    });
    this.bidService.getBids().subscribe(res => {
      var bidInd = res.findIndex(x => x.name === this.name);
      if (bidInd !== -1)
      this.user.mount = res[bidInd].mount;
    })
    this.socketService.socket.on('userBidInfo', (bidInfo: UserBid) => {
      console.info('bidInfo got', bidInfo)
      if (bidInfo.name == this.name)
        this.user.mount = bidInfo.mount;
    })
  }

  @Input() user: UserBid;
  @Input() name: String;

  placeBid(): void {
    this.bidService.placeBid(this.user.name, this.user.mount);
  }
}
