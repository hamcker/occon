import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socketServerUrl = 'http://localhost:4200';
  public socket;

  constructor() { 
    this.socket = new socketIo(this.socketServerUrl);
  }

  
}
