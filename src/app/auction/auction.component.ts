import { Component, OnInit } from '@angular/core';
import { BidService } from '../bid.service';
import { Auction } from '../models/auction';
import { UserBid } from '../models/userBid';
import { timer } from 'rxjs';
import { BalanceTime } from '../models/balanceTime';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.css']
})
export class AuctionComponent implements OnInit {

  constructor(private bidService: BidService) {
    this.auction = new Auction();
    this.auction.deadline = new Date('Oct 3, 2018, 6:00:00 PM');
    this.auction.highestUserBid = new UserBid();
    this.auction.balanceTime = new BalanceTime();
   }

  ngOnInit() {
    this.bidService.highestBid.subscribe(res => this.auction.highestUserBid = res);
    const balanceTimer = timer(0, 1000);
    balanceTimer.subscribe(() => {
      var milliseconds = Math.abs(this.auction.deadline.getTime() - (new Date()).getTime());
      this.auction.balanceTime = BalanceTime.fromMilliseconds(milliseconds);
    });
  }

  auction: Auction = new Auction();

}
