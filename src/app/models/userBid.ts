export class UserBid {
    name: String;
    mount: Number;
    offered: Date;
    isHighest: Boolean;
    payMount: Number;
}