import { Component, OnInit, Input } from '@angular/core';
import { BalanceTime } from '../models/balanceTime';

@Component({
  selector: 'app-balance-displayer',
  templateUrl: './balance-displayer.component.html',
  styleUrls: ['./balance-displayer.component.css']
})
export class BalanceDisplayerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input()
  balanceTime: BalanceTime;
}
